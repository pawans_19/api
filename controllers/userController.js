const userModel = require("../models/userModel");

exports.homepage = async (req, res) => {
  res.json({ message: "WELCOME TO THE API" });
};
exports.createUser = async (req, res) => {
  let user = await userModel.create(req.body);
  if (!user) res.status(400).json({ message: "user not created" });

  res.status(201).json({ message: "user created successfully", user });
};
exports.payment = async (req, res) => {
  let user = await userModel
    .findOne({ _id: req.body.id })
    .populate("ReferredUser");

  user.isPaymentMade = true;

  let referUser = await userModel.findOne({ _id: user.ReferredUser._id });
  referUser.TotalEarnings += 10;
  user.ReferredUser = null;
  await user.save();
  await referUser.save();
  res.status(201).json({ user, referUser });
};
