const mongoose = require("mongoose");

exports.dbConnection = () => {
  mongoose
    .connect(process.env.MONGODB_URL)
    .then(function () {
      console.log("Database connected successfully !");
    })
    .catch(function (err) {
      console.log(err.message);
    });
};
