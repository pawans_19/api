const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const userSchema = new Schema({
  Name: String,
  Email: String,
  ReferredUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "userModel",
    default: null,
  },
  isPaymentMade: {
    type: Boolean,
    defaule: false,
  },
  TotalEarnings: {
    type: Number,
    default: 0,
  },
});

module.exports = model("userModel", userSchema);
