var express = require("express");
const {
  homepage,
  createUser,
  payment,
} = require("../controllers/userController");
var router = express.Router();

/* GET / --gets the homepage. */
router.get("/", homepage);

/* POST /create --creates user */
router.post("/create", createUser);

/*POST /payment --makes payment */
router.post("/payment", payment);
module.exports = router;
